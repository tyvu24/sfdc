@isTest
public class ContinuationTestingForHttpRequest {
    public static testmethod void testWebService() {
        
        Account act = new Account (Name = 'test');
        insert act;
        PageReference pg = Page.testAccountAsyn;
        Test.setCurrentPage(pg);
        
        ISOtoCodeService controller = new ISOtoCodeService(new ApexPages.StandardController(act));
        
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation)controller.requestService();
        
        // Verify that the continuation has the proper requests
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        system.assert(requests.get(controller.returnedContinuationId) != null);
        
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');   
        // Set the fake response for the continuation     
        Test.setContinuationResponse(controller.returnedContinuationId, response);
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        // result is the return value of the callback
        System.assertNotEquals(null, result);
        // Verify that the controller's result variable
        //   is set to the mock response.
        System.assertEquals('Mock response body', controller.response);
    }
}