global class lookUpAccountAnnotation {
   @InvocableMethod
   public static List<String> getAccountIds(list<String> names) {
      List<Id> accountIds = new List<Id>();
      string sname = names.get(0); 
      List<Account> accounts = [SELECT Id FROM Account WHERE Name like :sname];
      for (Account account : accounts) {
         accountIds.add(account.Id);
      }
      return accountIds;
   }
}