public with sharing class ContactController 
{
@AuraEnabled
  public static list<Contact> getContacts() {
	List<Contact> lcontacts  = [SELECT ID, Name, MailingStreet, Phone, Email, LeadSource from Contact limit 10];
	return lcontacts;
  }
}