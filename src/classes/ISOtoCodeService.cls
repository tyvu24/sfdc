/**
*   @Author :   Jitendra Zaa
 *  @Web    :   http://JitendraZaa.com
 *
 * */
public class ISOtoCodeService {
    public String countryISO {get;set;}
    public String response {get;set;} 

    //public String txtTest {get ; set;}
    private String baseSericeURL = 'http://services.groupkt.com/country/get/iso2code/';
    public String returnedContinuationId ;
    private Account ac;
    public ISOtoCodeService(ApexPages.StandardController c)
    {
         ac = (Account)c.getrecord();
        countryISO = 'IN';
        
    } 

    /*
    @Future
    public static void getISOCode(list<String> lParams)
    {
        string sCode = lParams.get(0);
        string accID = lParams.get(1);
        Http ht = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(baseSericeURL+scode);
        HttpResponse res =  ht.send (req);
        //update back to account
        Account act = [SELECT ID,Description from Account where ID =:accID];
        act.Description = res.Body();
        
    }
    */
    
    public Object requestService(){

        //Timeout in seconds, 60 is limit
        Continuation con = new Continuation(60);

        // Set callback method
        con.continuationMethod='renderResponse';

        // Create callout request
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(baseSericeURL+countryISO);

        returnedContinuationId = con.addHttpRequest(req);
        
        countryISO='SG';
        return con;
    }

    public Object renderResponse() {
      // Get the response by using the unique label
      HttpResponse httpRes = Continuation.getResponse(returnedContinuationId);
      // Set the result variable that is displayed on the Visualforce page
      response = httpRes.getBody();
      // Return null to re-render the original Visualforce page
      countryISO='VN';
      //return null;
     Account b = [SELECT ID, Description from Account where ID = :ac.ID];
     b.Description = response;
     update b;
     
        return new PageReference('/' + ac.ID);
        
      
    }

}