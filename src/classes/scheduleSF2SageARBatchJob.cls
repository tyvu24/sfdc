global class scheduleSF2SageARBatchJob implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        SF2SageARBatchJob b = new SF2SageARBatchJob(); //ur batch class
        database.executebatch(b);          
    } 
}