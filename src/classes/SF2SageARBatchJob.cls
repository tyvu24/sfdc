global class SF2SageARBatchJob implements Database.Batchable<SObject>{
        
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, Name, Description FROM Account   WHERE Industry IN (\'Agriculture\')');
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for(Account acc : scope) {
        system.debug(acc.Website);
         /* do something */ }
    }

    global void finish(Database.BatchableContext bc)
    {
        
    }
    }