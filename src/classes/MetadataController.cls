public with sharing class MetadataController {
    
    public static String WRITE2LOCAL = 'LOCAL';
    public static String WRITE2REMOTE = 'REMOTE';
    public String selectedObject {get;set;}
    public String remoteUserName {get;set;}
    public String remotePassword {get;set;}
    
    public String remoteOrgSession ;
    public String remoteOrgEndPoint ;
    public map<String,MetaData__c> mapLocalMdata = new map<String,MetaData__c>();
    public map<String,Remote_MetaData__c> mapRemoteMdata = new map<String,Remote_MetaData__c>();
    
    public class DataBinding {
        
        public string sObjectName {get;set;}
        public list<FieldCompared> fieldList {get;set;} 
    }
    
    public class FieldCompared {
        
        public string sFieldName {get;set;}
        public string sFieldLabel {get;set;}
        public string localXML {get;set;}
        public string remoteXML {get;set;}
        public string localType {get;set;}
        public string remoteType {get;set;}
        public String selectedOverriden {get;set;}
    }
    
    public List<SelectOption> getObjects()
    {
      List<SelectOption> options = new List<SelectOption>();
            
      Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
      List<String> lobs = new List<String>(schemaMap.keySet());
        //fill in 
        for (String soj:lobs)
        {
             if (!schemaMap.get(soj).getDescribe().isCustomSetting() && schemaMap.get(soj).getDescribe().getRecordTypeInfos().size() > 0 
             && schemaMap.get(soj).getDescribe().isCreateable() &&
            !schemaMap.get(soj).getDescribe().getName().containsignorecase('history') && 
            !schemaMap.get(soj).getDescribe().getName().containsignorecase('tag') && !schemaMap.get(soj).getDescribe().getName().containsignorecase('share') 
            && !schemaMap.get(soj).getDescribe().getName().containsignorecase('feed'))
            {
                options.add(new SelectOption(soj, schemaMap.get(soj).getDescribe().getName()));
            }
            
        }
      
        //sort by alphabet
        options.sort();
        return options;
    }
    
    
    public String getObject() {
        return selectedObject;
    }

    public void setObject(String tObject) {
        this.selectedObject = tObject;
    }
    
    
    public List<SelectOption> getListOfOverriden()
    {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('NO_ACTION','NO_ACTION'));
      options.add(new SelectOption('Override2Local','Override2Local'));
      options.add(new SelectOption('Override2Remote','Override2Remote'));
      
       return options;
    }
    
    public DataBinding searchResult {get;set;}
    
    public void Connect2Orgs(){
        try
        {
            system.debug('###1:' + remoteOrgSession + '-' + selectedObject);
            if (remoteUserName==null || remotePassword==null)
            {
                //add apex error message
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Please input Remote\'s User and Password!');
                ApexPages.addMessage(errorMsg);
                
            }
            
            else if (remoteOrgSession==null && remoteUserName!=null && remotePassword!=null )
            {
                //1. Connect to remote org
                MetadataUtilities.login2Org(remoteUserName,remotePassword);
                remoteOrgSession=MetadataUtilities.REMOTE_SESSIONID;
                remoteOrgEndPoint = MetadataUtilities.REMOTE_ENDPOINT;
                remoteOrgEndPoint=remoteOrgEndPoint.left(remoteOrgEndPoint.indexOf('/services'));
                ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Connect successfully!');
                ApexPages.addMessage(successMsg);
                
            }
            else
            {
                //add apex error message
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Connection existed!');
                ApexPages.addMessage(errorMsg);
                
            }
        }
        catch (Exception e)
        {
            //add apex error message
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        return;
    }
    
    public void MetadataOverride(){
        transient list<String> insertedLocalFieldAndOrgs = new list<String>();
        transient list<String> updatedLocalFieldAndOrgs = new list<String>();
        transient list<String> insertedRemoteFieldAndOrgs = new list<String>();
        transient list<String> updatedRemoteFieldAndOrgs = new list<String>();
        
        try
        {
            if (remoteOrgSession==null || selectedObject==null || selectedObject=='')
            {
                //add apex error message
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Please connect to remote org and select an object!');
                ApexPages.addMessage(errorMsg);
                return; 
            }
            
            //iterate thru and find marked overriden requests
            for (FieldCompared fcp:searchResult.fieldList)
            {
                system.debug('### result:' + fcp);
                //for local org
                if (fcp.selectedOverriden=='Override2Local' )
                {
                    
                    if (fcp.localType==null || fcp.localType=='')
                    {
                        //is insert action
                        insertedLocalFieldAndOrgs.add(fcp.sFieldName + '###' + selectedObject +  '###' 
                                                        + WRITE2LOCAL);
                    }
                    else if (fcp.localType!=null && fcp.localType!='')
                    {
                        //is update action
                        updatedLocalFieldAndOrgs.add(fcp.sFieldName + '###' + selectedObject +  '###' 
                                                        + WRITE2LOCAL);
                    }
                    
                }
                //for remote org
                else if (fcp.selectedOverriden=='Override2Remote')
                {
                    if (fcp.remoteType==null || fcp.remoteType=='')
                    {
                        //is insert action
                        insertedRemoteFieldAndOrgs.add(fcp.sFieldName + '###' + selectedObject +  '###' 
                                                        + WRITE2REMOTE);
                    }
                    else if (fcp.remoteType!=null && fcp.remoteType!='')
                    {
                        //is update action
                        updatedRemoteFieldAndOrgs.add(fcp.sFieldName + '###' + selectedObject +  '###' 
                                                        + WRITE2REMOTE);
                    }
                }
                
            }
            
            //a. call the meta data api to insert/update selected fields: local - insert
            if (insertedLocalFieldAndOrgs.size()>0)
            {
                //a. for insert action on local  
                MetadataUtilities.ObjectDefinition objInsertedLocalData = FieldMapping(insertedLocalFieldAndOrgs);
                if (objInsertedLocalData!=null)
                {
                    MetadataUtilities.fieldCreation(objInsertedLocalData,MetadataUtilities.CREATE_OPERATION,MetadataUtilities.LOCAL_ENDPOINT,
                                                    MetadataUtilities.LOCAL_SESSIONID);
                }
            }
            
            //b. call the meta data api to insert/update selected fields: local - update
            if (updatedLocalFieldAndOrgs.size()>0)
            {
                //a. for insert action on local  
                MetadataUtilities.ObjectDefinition objUpdatedLocalData = FieldMapping(updatedLocalFieldAndOrgs);
                if (objUpdatedLocalData!=null)
                {
                    MetadataUtilities.fieldCreation(objUpdatedLocalData,MetadataUtilities.UPDATE_OPERATION,MetadataUtilities.LOCAL_ENDPOINT,
                                                    MetadataUtilities.LOCAL_SESSIONID);
                }
            }
            
            //c. call the meta data api to insert/update selected fields: remote - insert
            if (insertedRemoteFieldAndOrgs.size()>0)
            {
                //a. for insert action on local  
                MetadataUtilities.ObjectDefinition objInsertedRemoteData = FieldMapping(insertedRemoteFieldAndOrgs);
                if (objInsertedRemoteData!=null)
                {
                    MetadataUtilities.fieldCreation(objInsertedRemoteData,MetadataUtilities.CREATE_OPERATION,remoteOrgEndPoint,
                                                    remoteOrgSession);
                }
            }
            
            //d. call the meta data api to insert/update selected fields: remote - update
            if (updatedRemoteFieldAndOrgs.size()>0)
            {
                //a. for insert action on local  
                MetadataUtilities.ObjectDefinition objUpdatedRemoteData = FieldMapping(updatedRemoteFieldAndOrgs);
                if (objUpdatedRemoteData!=null)
                {
                    MetadataUtilities.fieldCreation(objUpdatedRemoteData,MetadataUtilities.UPDATE_OPERATION,remoteOrgEndPoint,
                                                    remoteOrgSession);
                }
            }
            
            //add apex success message
            ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Overriding successfully!');
            ApexPages.addMessage(successMsg);
        }
        catch (Exception e)
        {
            //add apex error message
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(errorMsg);
        }
        
        
    }
    
    private MetadataUtilities.ObjectDefinition FieldMapping(list<String> listFieldNames){
        MetadataUtilities.ObjectDefinition objData = new MetadataUtilities.ObjectDefinition();
        list<MetadataUtilities.FieldDefinition> lstFieldDef = new list<MetadataUtilities.FieldDefinition>(); 
        
        try
        {
            
            //get the list of fields from the map
            for (String strField:listFieldNames)
            {
                list<String> sParams = strField.split('###');
                MetadataUtilities.FieldDefinition fDef = new MetadataUtilities.FieldDefinition();
                //if override to local => get source from remote data
                if (sparams[2]==WRITE2LOCAL )
                {
                    Remote_MetaData__c mdt= mapRemoteMdata.get(sparams[0] + '###' + sparams[1] );
                    //fillin all the fields from remote metadata
                    objData.objectName =mdt.Name;
                    fDef.formula=mdt.formula__c;
                    fDef.defaultValue=mdt.defaultValue__c;
                    fDef.lookupFilter=mdt.lookupFilter__c;
                    fDef.filterItems=mdt.filterItems__c;
                    fDef.inlineHelpText=mdt.inlineHelpText__c;
                    fDef.label=mdt.label__c;
                    fDef.length=Integer.valueof(mdt.length__c);
                    fDef.fullName=mdt.fullName__c;
                    fDef.maskChar=mdt.maskChar__c;
                    fDef.maskType=mdt.maskType__c;
                    fDef.precision=integer.valueof(mdt.precision__c);
                    fDef.referenceTargetField=mdt.referenceTargetField__c;
                    fDef.relationshipName=mdt.relationshipName__c;
                    fDef.relationshipOrder=integer.valueof(mdt.relationshipOrder__c);
                    fDef.scale=integer.valueof(mdt.scale__c);
                    fDef.type=mdt.type__c;
                    fDef.encrypted=mdt.encrypted__c;
                    fDef.externalId=mdt.externalId__c;
                    fDef.unique=mdt.unique__c;
                    fDef.caseSensitive=mdt.caseSensitive__c;
                    fDef.displayFormat=mdt.displayFormat__c;
                    fDef.description=mdt.description__c;
                    fDef.formulaTreatBlanksAs=mdt.formulaTreatBlanksAs__c;
                    fDef.populateExistingRows=mdt.populateExistingRows__c;
                    fDef.referenceTo=mdt.referenceTo__c;
                    fDef.relationshipLabel=mdt.relationshipLabel__c;
                    fDef.reparentableMasterDetail=mdt.reparentableMasterDetail__c;
                    fDef.required=mdt.required__c;
                    fDef.restrictedAdminField=mdt.restrictedAdminField__c;
                    fDef.startingNumber=integer.valueof(mdt.startingNumber__c);
                    fDef.summarizedField=mdt.summarizedField__c;
                    fDef.summaryFilterItems=mdt.summaryFilterItems__c;
                    fDef.summaryForeignKey=mdt.summaryForeignKey__c;
                    fDef.summaryOperation=mdt.summaryOperation__c;
                    fDef.trackFeedHistory=mdt.trackFeedHistory__c;
                    fDef.trackHistory=mdt.trackHistory__c;
                    fDef.trackTrending=mdt.trackTrending__c;
                    fDef.visibleLines=integer.valueof(mdt.visibleLines__c);
                    fDef.deleteConstraint=mdt.deleteConstraint__c;
                    fDef.displayLocationInDecimal=mdt.displayLocationInDecimal__c;
                    fDef.ValueSet=mdt.ValueSet__c;
                    fDef.ValueSetValuesDefinition=mdt.ValueSetValuesDefinition__c;
                    fDef.ValueSettings =mdt.ValueSettings__c ;
                }
                else
                {
                    system.debug ('here map local:' + mapLocalMdata);
                    MetaData__c mdt= mapLocalMdata.get(sparams[0] + '###' + sparams[1]);
                    //fillin all the fields from local metadata
                    objData.objectName =mdt.Name;
                    fDef.formula=mdt.formula__c;
                    fDef.defaultValue=mdt.defaultValue__c;
                    fDef.lookupFilter=mdt.lookupFilter__c;
                    fDef.filterItems=mdt.filterItems__c;
                    fDef.inlineHelpText=mdt.inlineHelpText__c;
                    fDef.label=mdt.label__c;
                    fDef.length=Integer.valueof(mdt.length__c);
                    fDef.fullName=mdt.fullName__c;
                    fDef.maskChar=mdt.maskChar__c;
                    fDef.maskType=mdt.maskType__c;
                    fDef.precision=integer.valueof(mdt.precision__c);
                    fDef.referenceTargetField=mdt.referenceTargetField__c;
                    fDef.relationshipName=mdt.relationshipName__c;
                    fDef.relationshipOrder=integer.valueof(mdt.relationshipOrder__c);
                    fDef.scale=integer.valueof(mdt.scale__c);
                    fDef.type=mdt.type__c;
                    fDef.encrypted=mdt.encrypted__c;
                    fDef.externalId=mdt.externalId__c;
                    fDef.unique=mdt.unique__c;
                    fDef.caseSensitive=mdt.caseSensitive__c;
                    fDef.displayFormat=mdt.displayFormat__c;
                    fDef.description=mdt.description__c;
                    fDef.formulaTreatBlanksAs=mdt.formulaTreatBlanksAs__c;
                    fDef.populateExistingRows=mdt.populateExistingRows__c;
                    fDef.referenceTo=mdt.referenceTo__c;
                    fDef.relationshipLabel=mdt.relationshipLabel__c;
                    fDef.reparentableMasterDetail=mdt.reparentableMasterDetail__c;
                    fDef.required=mdt.required__c;
                    fDef.restrictedAdminField=mdt.restrictedAdminField__c;
                    fDef.startingNumber=integer.valueof(mdt.startingNumber__c);
                    fDef.summarizedField=mdt.summarizedField__c;
                    fDef.summaryFilterItems=mdt.summaryFilterItems__c;
                    fDef.summaryForeignKey=mdt.summaryForeignKey__c;
                    fDef.summaryOperation=mdt.summaryOperation__c;
                    fDef.trackFeedHistory=mdt.trackFeedHistory__c;
                    fDef.trackHistory=mdt.trackHistory__c;
                    fDef.trackTrending=mdt.trackTrending__c;
                    fDef.visibleLines=integer.valueof(mdt.visibleLines__c);
                    fDef.deleteConstraint=mdt.deleteConstraint__c;
                    fDef.displayLocationInDecimal=mdt.displayLocationInDecimal__c;
                    fDef.ValueSet=mdt.ValueSet__c;
                    fDef.ValueSetValuesDefinition=mdt.ValueSetValuesDefinition__c;
                    fDef.ValueSettings =mdt.ValueSettings__c ;
                }
                
                lstFieldDef.add(fDef);
            }
            
            objData.listFields = lstFieldDef;
            
        }
        catch (Exception e)
        {
            
        }
        return objData;
    }
    public void MetadataSearch()
    {
        try
        {
            system.debug('###2:' + remoteOrgSession + '-' + selectedObject);
            if (remoteOrgSession==null || selectedObject==null || selectedObject=='')
            {
                //add apex error message
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Please connect to remote org and select an object!');
                ApexPages.addMessage(errorMsg);
                return; 
            }
            
            system.debug('here:' + selectedObject + '-' + remoteOrgEndPoint + '-' + remoteOrgSession);
            if (selectedObject!=null && selectedObject!='')
            {
                //1.load metadata to metadata object - local
                transient list<MetaData__c> localMdata = MetadataUtilities.MetadataExtracting(new String[] {selectedObject},
                                                    MetadataUtilities.LOCAL_ENDPOINT,MetadataUtilities.LOCAL_SESSIONID);
                
                //2. load metadata to remote metata object - local
                transient list<Remote_MetaData__c> remoteMdata = MetadataUtilities.RemoteMetadataExtracting(new String[] {selectedObject},
                                                    remoteOrgEndPoint,remoteOrgSession);
                
                
                //3. convert the 2 above lists to maps
                for (MetaData__c mdt:localMdata)
                {
                    mapLocalMdata.put(mdt.fullName__c.replace('tvj__','') + '###' +mdt.Name.replace('tvj__','') , mdt);
                }
                
                for (Remote_MetaData__c rmdt:remoteMdata)
                {
                    mapRemoteMdata.put(rmdt.fullName__c + '###' +rmdt.Name , rmdt);
                }
                
                //binding and show on vf page
                searchResult= new DataBinding();
                searchResult.sObjectName = selectedObject;
                transient list<FieldCompared> lFields = new list<FieldCompared>();
                system.debug('local@@:' +localMdata);
                system.debug('remote@@:' +remoteMdata);
                
                //fillin result based on local mdata
                for (MetaData__c localMD:localMdata)
                {
                    FieldCompared fCompared = new FieldCompared();
                    fCompared.sFieldName=localMD.fullName__c;
                    fCompared.sFieldLabel=localMD.label__c;
                    fCompared.localXML = localMD.XML__c;
                    fCompared.localType = localMD.Type__c;
                    for (Remote_MetaData__c remoteMD:remoteMdata)
                    {
                        if (localMD.fullName__c.replace('tvj__','')==remoteMD.fullName__c)
                        {
                            fCompared.remoteXML=remoteMD.XML__c;
                            fCompared.remoteType=remoteMD.Type__c;
                        }   
                    }
                    
                    lFields.add(fCompared);
                    
                }
                
                //fillin data based on remote data which is not mapped with local data
                for (Remote_MetaData__c remoteMD:remoteMdata)
                {
                    Boolean isMapped=false;
                    for (MetaData__c localMD:localMdata)
                    {
                        if (localMD.fullName__c.replace('tvj__','')==remoteMD.fullName__c)
                        {
                            isMapped=true;
                            break;
                        }   
                    }
                    
                    //if this field is new from remote org
                    if (isMapped==false)
                    {
                        FieldCompared fCompared = new FieldCompared();
                        fCompared.sFieldName=remoteMD.fullName__c;
                        fCompared.sFieldLabel=remoteMD.label__c;
                        fCompared.remoteXML = remoteMD.XML__c;
                        fCompared.remoteType = remoteMD.Type__c;
                        
                        lFields.add(fCompared);
                    }
                    
                    
                }
                
                //assign to list binding
                searchResult.fieldList = lFields;   
                
            }
            
            //show successful message;
            ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Extracting successfully!');
            ApexPages.addMessage(successMsg);
        }
        catch (Exception e)
        {
            system.debug (e.getLineNumber() + '-' + e.getMessage());
            //add apex error message
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(errorMsg);         
        }
        
    }
  
}