global class tmpBatch implements Database.Batchable<sObject>
{
  
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        return Database.getQueryLocator('SELECT ID FROM ACCOUNT');
    }
    
    global void execute (Database.BatchableContext BC, list<sObject> scope)
    {
        for (sObject sso:scope)
        {
            system.debug(sso);
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
}