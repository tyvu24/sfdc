@istest 
private class opportunityProductEntryTests {


    static testMethod void theTests(){
        
        // You really should create test data, but I'm going to query instead
        // It's my best shot of avoiding a test failure in most orgs
        // Once you've installed this package though, you might want to write your own tests
        // or at least customize these ones to make them more applicable to your org
		Account act = new Account (Name='ACg');
		insert act;

		Id pricebookId = Test.getStandardPricebookId();
		
		Product2 prod1 = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod1;
			
		Product2 prod2 = new Product2(Name = 'Laptop X4OO', 
            Family = 'Hardware');
        insert prod2;

		Product2 prod3 = new Product2(Name = 'Laptop X600', 
            Family = 'Hardware');
        insert prod3;	
		
		PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice1;

		PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod2.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice2;

		PricebookEntry standardPrice3 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod3.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice3;	
		
		// Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
		PricebookEntry csp1 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp1;

		PricebookEntry csp2 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod2.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp2;
		
		PricebookEntry csp3 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod3.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp3;
		
		
		Opportunity oppty = new Opportunity (AccountID=act.ID,Name='test oppty', StageName='Negotiation/Review', CloseDate = date.today());
		insert oppty;	
		
        OpportunityLineItem oli =  new OpportunityLineItem (PricebookEntryId = csp1.ID,
					OpportunityId=oppty.ID,Quantity=1,UnitPrice=1000);
		insert oli;
               
                
        ////////////////////////////////////////
        //  test opportunityProductEntry
        ////////////////////////////////////////
        
        // load the page       
        PageReference pageRef = Page.opportunityProductEntry;
        pageRef.getParameters().put('Id',oli.OpportunityId);
        Test.setCurrentPageReference(pageRef);
        
        // load the extension
        opportunityProductEntryExtension oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oppty));
        
        // test 'getChosenCurrency' method
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');

        // we know that there is at least one line item, so we confirm
        Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oPEE.searchString = 'michaelforce is a hip cat';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
        //test remove from shopping cart
        oPEE.toUnselect = oli.PricebookEntryId;
        oPEE.removeFromShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        //test save and reload extension
        oPEE.onSave();
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oppty));
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        // test search again, this time we will find something
        oPEE.searchString = oli.PricebookEntry.Name;
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()>0);       

        // test add to Shopping Cart function
        oPEE.toSelect = oPEE.AvailableProducts[0].Id;
        oPEE.addToShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount);
                
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPEE.onSave();
        system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(OpportunityLineItem o : oPEE.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE.onSave();
        
        // query line items to confirm that the save worked
        opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Pipeline',Amount=10,closeDate=System.Today()+30,AccountId=oli.Opportunity.AccountId
							);
        insert(newOpp);
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(newOpp));
        System.assert(oPEE.priceBookCheck()==null);
        
		//test change pricebook when oppty closed won
		newOpp.StageName='Closed Won';
		update newOpp;	
		
		oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(newOpp));
		oPEE.theBook=new Pricebook2();
		system.debug('test222');
        System.assert(oPEE.priceBookCheck()==null);
		
        // final quick check of cancel button
        System.assert(oPEE.onCancel()!=null);
        
        
        ////////////////////////////////////////
        //  test redirect page
        ////////////////////////////////////////
        
        // load the page
        pageRef = Page.opportunityProductRedirect;
        pageRef.getParameters().put('Id',oli2[0].Id);
        Test.setCurrentPageReference(pageRef);

        // load the extension and confirm that redirect function returns something
        opportunityProductRedirectExtension oPRE = new opportunityProductRedirectExtension(new ApexPages.StandardController(oli2[0]));
        System.assert(oPRE.redirect()!=null);
     
    }
	
	
    static testMethod void theTestMultiplePBs(){
        
        // You really should create test data, but I'm going to query instead
        // It's my best shot of avoiding a test failure in most orgs
        // Once you've installed this package though, you might want to write your own tests
        // or at least customize these ones to make them more applicable to your org
		Account act = new Account (Name='ACg');
		insert act;

		Id pricebookId = Test.getStandardPricebookId();
		
		Product2 prod1 = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod1;
			
		Product2 prod2 = new Product2(Name = 'Laptop X4OO', 
            Family = 'Hardware');
        insert prod2;

		Product2 prod3 = new Product2(Name = 'Laptop X600', 
            Family = 'Hardware');
        insert prod3;	
		
		PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice1;

		PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod2.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice2;

		PricebookEntry standardPrice3 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod3.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice3;	
		
		// Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
		PricebookEntry csp1 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp1;

		PricebookEntry csp2 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod2.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp2;
		
		PricebookEntry csp3 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = prod3.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp3;
		
		// Create a custom price book
        Pricebook2 customPB2 = new Pricebook2(Name='Custom Pricebook2', isActive=true);
        insert customPB2;
        
		PricebookEntry csp4 = new PricebookEntry(
            Pricebook2Id = customPB2.ID, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert csp4;
		
		Opportunity oppty = new Opportunity (AccountID=act.ID,Name='test oppty', StageName='Negotiation/Review', CloseDate = date.today());
		insert oppty;	
		
        OpportunityLineItem oli =  new OpportunityLineItem (PricebookEntryId = csp1.ID,
					OpportunityId=oppty.ID,Quantity=1,UnitPrice=1000);
		insert oli;
               
                
        ////////////////////////////////////////
        //  test opportunityProductEntry
        ////////////////////////////////////////
        
        // load the page       
        PageReference pageRef = Page.opportunityProductEntry;
        pageRef.getParameters().put('Id',oli.OpportunityId);
        Test.setCurrentPageReference(pageRef);
        
        // load the extension
        opportunityProductEntryExtension oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oppty));
        
        // test 'getChosenCurrency' method
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');

        // we know that there is at least one line item, so we confirm
        Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oPEE.searchString = 'michaelforce is a hip cat';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
        //test remove from shopping cart
        oPEE.toUnselect = oli.PricebookEntryId;
        oPEE.removeFromShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        //test save and reload extension
        oPEE.onSave();
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oppty));
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        // test search again, this time we will find something
        oPEE.searchString = oli.PricebookEntry.Name;
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()>0);       

        // test add to Shopping Cart function
        oPEE.toSelect = oPEE.AvailableProducts[0].Id;
        oPEE.addToShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount);
                
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPEE.onSave();
        system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(OpportunityLineItem o : oPEE.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE.onSave();
        
        // query line items to confirm that the save worked
        opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Pipeline',Amount=10,closeDate=System.Today()+30,AccountId=oli.Opportunity.AccountId
							);
        insert(newOpp);
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(newOpp));
        System.assert(oPEE.priceBookCheck()!=null);
        
		// final quick check of cancel button
        System.assert(oPEE.onCancel()!=null);
        
        
        ////////////////////////////////////////
        //  test redirect page
        ////////////////////////////////////////
        
        // load the page
        pageRef = Page.opportunityProductRedirect;
        pageRef.getParameters().put('Id',oli2[0].Id);
        Test.setCurrentPageReference(pageRef);

        // load the extension and confirm that redirect function returns something
        opportunityProductRedirectExtension oPRE = new opportunityProductRedirectExtension(new ApexPages.StandardController(oli2[0]));
        System.assert(oPRE.redirect()!=null);
     
    }
	
	
	 static testMethod void theTestAbove100Pds(){
        
        // You really should create test data, but I'm going to query instead
        // It's my best shot of avoiding a test failure in most orgs
        // Once you've installed this package though, you might want to write your own tests
        // or at least customize these ones to make them more applicable to your org
		Account act = new Account (Name='ACg');
		insert act;

		Id pricebookId = Test.getStandardPricebookId();
		
		list<Product2> lpd = new list<Product2>();
		
		for (Integer i = 0; i < 101; i++) {
			Product2 prod = new Product2(Name = 'Laptop X200-' + string.valueof(i), 
            Family = 'Hardware');
			lpd.add(prod);
		}
		
		insert lpd;
		
		list<PricebookEntry> lpen = new list<PricebookEntry>();
		
		for (Integer i = 0; i < 101; i++) {
			PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = lpd.get(i).ID,
            UnitPrice = 10000, IsActive = true);
			
			lpen.add(standardPrice1);
		}
		
		insert lpen;
		
		
		// Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
		list<PricebookEntry> lpen2 = new list<PricebookEntry>();
		
		for (Integer i = 0; i < 101; i++) {
			PricebookEntry csp1 = new PricebookEntry(
            Pricebook2Id = customPB.ID, Product2Id = lpd.get(i).ID,
            UnitPrice = 10000, IsActive = true);
			
			lpen2.add(csp1);
		}
		
		insert lpen2;
		
		
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Pipeline',Amount=10,closeDate=System.Today()+30,AccountId=act.ID
							);
        insert(newOpp);
        opportunityProductEntryExtension oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(newOpp));
        system.assertEquals(oPEE.overLimit,true);
		//System.assert(oPEE.priceBookCheck()!=null);
        
     
    }
}