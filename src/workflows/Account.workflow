<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>testoutbound</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://services.groupkt.com</endpointUrl>
        <fields>AccountSource</fields>
        <fields>AnnualRevenue</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>ty.vudinh@gmail.com</integrationUser>
        <name>testoutbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
